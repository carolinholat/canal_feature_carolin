# Flood\CanalEEEEEEEEEEEE

Folder structure for a [Flood\Canal](https://bitbucket.org/bemit_eu/canal) project.

Flood Project: Canal, which consists of:

- [Canal Lib](https://bemit.codes/canal) - [BitBucket](https://bitbucket.org/bemit_eu/canal)
- [Canal: Asset](https://bemit.codes/canal-asset) - [BitBucket](https://bitbucket.org/bemit_eu/canal-asset)
- [Canal: View](https://bemit.codes/canal-view) - [BitBucket](https://bitbucket.org/bemit_eu/canal-view)
- [Canal: Structure](https://bemit.codes/canal-structure) - [BitBucket](https://bitbucket.org/bemit_eu/canal-structure)
- bundled frontend and admin [Canal Bundle](https://bemit.codes/canal-bundle) - [BitBucket](https://bitbucket.org/bemit_eu/canal-bundle)
- only admin [Canal: Admin](https://bemit.codes/canal-admin) - [BitBucket](https://bitbucket.org/bemit_eu/canal-admin)

Setup the Flood\Canal PHP framework with composer:

    composer create-project flood/canal-structure 
    
    composer create-project flood/canal-structure <path> <stability>
    
    for develop:  
    composer create-project -s DEV flood/canal-structure:dev-develop canal
    
Install admin separately or use canal-bundle.

# How-To

Docu and Guide:

- [Setup and Run](https://painttheweb.de/flood-canal/setup-and-run-a-project)
- [View and Templates](https://painttheweb.de/flood-canal/templating)
- [Content Management](https://painttheweb.de/flood-canal/content-management)
- [Asset Management](https://painttheweb.de/flood-canal/asset-management-for-sass-and-js-files)
- [CLI, Console](https://painttheweb.de/flood-canal/console-cli-mode) - add users, clear caches

## Quick Start Development

- needs `NodeJS, npm, bower, PHP`
    - on windows also `WSL Ubuntu with PHP, above dependencies in windows except PHP`
    
After installing through composer, these steps are all for starting development:

```bash
# Installs all developer dependencies for Grunt, WebPack (ES6), Admin, Asset Files
npm i

# Starts PHP Dev Server
# For Windows
npm run start-win
# For Linux/macOS
npm run start-uni

# For re-building all
npm run build

# For starting `assets/` watcher
npm run watch-grunt # Sass, Media, ES5
npm run watch-wp # ES6

# Create User for Admin Access
php cli user create demo -a demo
php cli user create <name> -a <pw>
```

Now go to:

- for Frontend: [http://localhost:25020/](http://localhost:25020/)

## Run on Server

After creating the project with composer, just drop the files to a `PHP 7.2+` webspace and call the URL. You should see an indication that the page has been run.
 
- see `app/DemoController.php` how a simple controller could be designed
- see `view/_meta.twig` how a simple view/template file could be designed
- in `_config.php`, `_content.php` and `_route.php` routes and config are set, register new controllers, everything is run by `flow.php`

- for Frontend: https://your-domain.tld

# Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

# Copyright

2017-2019 | [bemit UG (haftungsbeschränkt)](https://bemit.eu) - project@bemit.codes

Maintainer: [Michael Becker](https://mlbr.xyz)