<?php

namespace Flood\Canal\App;

use Flood\Canal\AppService\PageController;

class Law extends PageController {
    /**
     * Home constructor.
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
        $this->path_template = 'Law.twig';

        /*
         * A demo AppService.method
         */
        $this->view();
    }
}