<?php

namespace Flood\Canal\App\Api;

use Flood\Canal\Controller\Base;

class TemplateFetch extends Base {
    protected $error = false;
    /**
     * @var string will be added before the template name, when false is not gonna be added
     */
    public $src_folder = 'templatefetch/';

    /**
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);

        if (empty($frontend->match['template_id']) || !is_string($frontend->match['template_id'])) {
            $this->error = true;
            echo json_encode([
                'error' => 'TemplateFetch: template_id empty',
            ]);
        } else if (!empty($frontend->match['template_id']) && is_string($frontend->match['template_id'])) {
            $this->addHeader('Content-Type: application/json');
            $this->path_template = (false !== $this->src_folder ? $this->src_folder : '') . $frontend->match['template_id'] . '.twig';
        }
    }

    public function respond($template = null, $assign_default = true) {
        $arr = filter_input_array(INPUT_GET, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        //$_GET['d']='ds';
        //echo json_encode($_GET);
        //echo json_encode($arr);
        foreach ($arr as $k => $v) {
            // todo: fix urldecode in js
            // k: the key isn't decoded from js right now
            // v: is encoded from js, but also traversed from sanitize, so the html chars need to be reversed
            $this->assign(urldecode($k), html_entity_decode(urldecode($v)));
        }
        if (true !== $this->error) {
            echo json_encode([
                'html'  => parent::respond($template, $assign_default),
                'id'    => $this->frontend->match['template_id'],
                'error' => false,
            ]);
        }
    }
}