<?php

namespace Flood\Canal\App;

use Flood\Canal\AppService\PageController;

class Contact extends PageController {

    /**
     * Home constructor.
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
        $this->path_template = 'Contact.twig';

        /*
         * A demo AppService.method
         */
        $this->view();
    }

    public function handleRequest($mailer_option) {
        if (filter_has_var(INPUT_POST, 'action')) {
            $this->assign('contact_error', true);

            if (filter_has_var(INPUT_POST, 'name')) {
                $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            }

            if (filter_has_var(INPUT_POST, 'email')) {
                $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            }

            if (filter_has_var(INPUT_POST, 'tel')) {
                $tel = filter_input(INPUT_POST, 'tel', FILTER_SANITIZE_STRING);
            }

            if (filter_has_var(INPUT_POST, 'msg')) {
                $msg = filter_input(INPUT_POST, 'msg', FILTER_SANITIZE_STRING);
            }

            if ($name && $email && $tel && $msg) {
                // Create the Transport
                try {
                    $msg = date('Y-m-d H:m:s') . ' - New Message from your Homepage' . "\r\n" . 'Name: ' . $name . "\r\n" . 'E-Mail: ' . $email . "\r\n" . 'Phone: ' . $tel . "\r\n" . 'Message: ' . $msg . "\r\n";
                    $transport = (new \Swift_SmtpTransport($mailer_option['host'], $mailer_option['port'], $mailer_option['security']))
                        ->setUsername($mailer_option['usr'])
                        ->setPassword($mailer_option['pwd']);

                    $mailer = new \Swift_Mailer($transport);

                    $message = (new \Swift_Message('[HP] ContactForm'))
                        ->setFrom([$mailer_option['usr'] => 'ContactForm'])
                        ->setTo([$mailer_option['to']])
                        ->setBody($msg);

                    $result = $mailer->send($message);
                } catch (\Swift_TransportException $e) {
                    if ($this->frontend->debug) {
                        echo 'TransportException: ' . $e->getMessage() . "\r\n";
                    }
                }
                if (0 < $result) {
                    $this->assign('contact_error', false);
                }
            } else {
                $this->assign('contact_error', 'missing-req');
            }
        }
    }
}