<?php

namespace Flood\Canal\AppService;

use Flood\Canal\Controller\Content;
use Flood\Canal\Service\Gdpr\Gdpr;
use Flood\Canal\Service\LinkList;

class PageController extends Content {

    /**
     * Call from actual controller to have the nav vars on every page you have
     */
    public function view() {

        $nav_header_link_list = new LinkList();
        $nav_header_link_list->addLink('Home', $this->frontend->url_generator->generate('home'));
        $nav_header_link_list->addLink('About', $this->frontend->url_generator->generate('about'));
        $nav_header_link_list->addLink('SocialMedia', $this->frontend->url_generator->generate('socialmedia'));
        $this->assign('nav_header', $nav_header_link_list);

        $nav_service_link_list = new LinkList();
        $nav_service_link_list->addLink('Contact', $this->frontend->url_generator->generate('contact'));
        $nav_service_link_list->addLink('Imprint', $this->frontend->url_generator->generate('law', ['page' => 'imprint']));
        $nav_service_link_list->addLink('Privacy', $this->frontend->url_generator->generate('law', ['page' => 'privacy']));
        $this->assign('nav_service', $nav_service_link_list);

        $gdpr = new Gdpr([
            'global'       => [

                'google-maps' => [
                    'border'    => true,
                    'transform' => 'iframe',
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to Google.',
                            'req'            => 'required',
                        ],
                        'btn'   => 'show Map',
                    ],
                    'loading'   => [
                        'label' => 'The content is loading',
                    ],
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                /*
                 * Media, Fun
                 */
                'giphy'       => [
                    'border'    => true,
                    'data-attr' => [
                        'mozallowfullscreen',
                        'webkitallowfullscreen',
                        'allowfullscreen',
                    ],
                    'barrier'   => [
                        'label' => [
                            'value'    => 'showing will connect to Giphy',
                            'position' => 'before-check',
                        ],
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'pinterest' => [
                    'border'    => true,
                    'attr'      => [
                        'data-pin-width'        => 'large',
                        'data-pin-board-width'  => '150',
                        'data-pin-scale-height' => '800',
                        'data-pin-scale-width'  => '60',
                    ],
                    'barrier'   => [
                        'label' => [
                            'value'    => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to Pinterest.',
                            'position' => 'before-btn',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'tumblr' => [
                    'border'    => true,
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to Tumblr.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                '500px'   => [
                    'border'    => true,
                    'barrier'   => [
                        'btn' => 'show Image<br><small>will connect to 500px</small>',
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],


                /*
                 * Social Media Platforms
                 */
                'twitter' => [
                    'border'    => true,
                    'barrier'   => [
                        'icon'  => true,
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to Twitter.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'instagram' => [
                    'border'    => true,
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to Instagram.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'facebook' => [
                    'border'    => true,
                    'barrier'   => [
                        'icon'  => true,
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to Facebook.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                /*
                * Video Platforms
                */
                'youtube'  => [
                    'border'    => true,
                    'data-attr' => [
                        'allow' => 'autoplay; encrypted-media',
                        'allowfullscreen',
                    ],
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to YouTube.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'vimeo'   => [
                    'border'    => true,
                    'data-attr' => [
                        'mozallowfullscreen',
                        'webkitallowfullscreen',
                        'allowfullscreen',
                    ],
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to Vimeo.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                /*
                 * Audio Platforms
                 */
                'spotify' => [
                    'border'    => true,
                    'data-attr' => [
                        'allowtransparency' => 'true',
                        'allow'             => 'encrypted-media',
                    ],
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to Spotify.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'deezer' => [
                    'border'    => true,
                    'data-attr' => [
                        'data-allowTransparency' => 'true',
                    ],
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to Deezer.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'office365' => [
                    'border'    => true,
                    'data-attr' => [
                    ],
                    'barrier'   => [

                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'jsfiddle' => [
                    'border'    => true,
                    'data-attr' => [
                        'data-allowTransparency' => 'true',
                    ],
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to JSFiddle.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'sassmeister' => [
                    'border'    => true,
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to SassMeister.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'codepen' => [
                    'border'    => true,
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to CodePen.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'github' => [
                    'border'    => true,
                    'barrier'   => [
                        'check' => [
                            'type'           => 'checkbox',
                            'input_position' => 'before',
                            'label'          => 'I accept the <a href="' . $this->frontend->url_generator->generate('law', ['page' => 'privacy']) . '" target="_blank">privacy policy</a>, showing will connect to GitHub.',
                            'req'            => 'required',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],

                'fake-embed' => [
                    'barrier'   => [
                        'label' => [
                            'position' => 'before-btn',
                        ],
                    ],
                    'loading'   => true,
                    'prerender' => true,
                    'usr_link'  => [
                        'txt' => 'original Content ⮊',
                    ],
                ],
            ],
            'element_list' => [
                /*[
                    'family' => 'twitter',
                    'type'   => 'tweet',
                ],*/
            ],
        ]);

        $this->assign('gdpr_setting', $gdpr);
    }
}