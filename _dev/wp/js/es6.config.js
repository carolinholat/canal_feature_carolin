const path = require('path');

const PROD = process.env.NODE_ENV === 'production';
const ENV = process.env.NODE_ENV || 'development';

const path_root = '../../';
const path_root_out = '../../../';

let entry_point = {
    [path_root_out + 'data/out/js']: path_root + 'asset/js/execute-es6'
};

const config = {
    //context: path.resolve(__dirname, context),
    mode: 'development',
    entry: entry_point,
    output: {
        filename: '[name].min.js',
        path: path.resolve(__dirname)
    },
    devtool: 'source-map',
    // externals are needed for serverTS, as node modules must not be included, todo test if compatible with bable es6 includes like jquery
    // externals: nodeModules,
    module: {
        rules: [{
            // Babel ES6
            test: /\.js$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    plugins: [
                        'transform-es2015-template-literals',
                        // es6 promise polyfill, only rewrites on-demand
                        'es6-promise'
                    ],
                    presets: [
                        ['env']
                    ]
                }
            }]
        }]
    },
    plugins: [
        //new webpack.optimize.UglifyJsPlugin(),
    ]
};

module.exports = config;