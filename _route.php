<?php

use Flood\Canal\App;

/**
 * @param \Flood\Canal\Route\Routing $route
 */
return function($route) {
    $route->addList([
        'home'              => [
            'path'       => '/',
            'controller' => function($frontend) {
                $controller = new App\Home($frontend);

                return $controller->respond();
            },
        ],
        'about'             => [
            'path'       => '/about',
            'controller' => function($frontend) {
                $controller = new \Flood\Canal\AppService\PageController($frontend);
                $controller->view();
                return $controller->respond('About.twig');
            },
        ],
        'contact'           => [
            'path'       => '/contact',
            'controller' => function($frontend) {
                $controller = new App\Contact($frontend);
                $controller->handleRequest([
                    'host'     => 'mail.example.org',
                    'port'     => '465',
                    'security' => 'ssl',
                    'usr'      => 'info@example.org',
                    'pwd'      => 'some-password',
                    'to'       => 'recipient@example.org',
                ]);

                return $controller->respond();
            },
            'defaults'   => ['rel' => 'nofollow, noindex'],
        ],
        'socialmedia'       => [
            'path'       => '/social-media',
            'controller' => function($frontend) {
                $controller = new App\SocialMedia($frontend);

                return $controller->respond();
            },
            'defaults'   => ['rel' => 'nofollow, noindex'],
        ],
        'law'               => [
            'path'         => '/law/{page}',
            'controller'   => function($frontend) {
                $controller = new App\Law($frontend);

                return $controller->respond();
            },
            'defaults'     => ['rel' => 'nofollow, noindex'],
            'requirements' => ['page' => 'imprint|privacy'],
        ],
        '404'               => [
            'path'       => '/{page}',
            'controller' => function($frontend) {
                $controller = new App\Error($frontend);

                return $controller->respond();
            },
            'defaults'   => ['rel' => 'nofollow, noindex'],
        ],
        /*
        'sitemap-xml' => [
            'path' => '/sitemap.xml',
            'controller' => function ($frontend) {
                $controller = new Sitemap($frontend);
                return $controller->response();
            },
        ],
        'rss' => [
            'path' => '/rss',
            'controller' => function ($frontend) {
                $controller = new Rss($frontend);
                return $controller->response();
            },
        ],*/
        'api-templatefetch' => [
            'path'       => '/api/templatefetch/{template_id}',
            'controller' => function($frontend) {
                $controller = new App\Api\TemplateFetch($frontend);
                $controller->respond();

                return '';
            },
            'defaults'   => ['rel' => 'nofollow, noindex'],
        ],
    ]);
};