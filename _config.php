<?php

use Flood\Canal\Feature\User\Token;

/**
 * @param $frontend \Flood\Canal\Frontend
 */
return function($frontend) {
    $frontend->debug = true;

    /**
     * @var string $host only allow routes from this host, when not another is defined within routes. ['host'=>'val']
     */
    $frontend->host = '';
    // when using dev server ssl is off, when production it is on
    $frontend->ssl = (php_sapi_name() == 'cli-server' ? false : true);

    $frontend->path_tmp = __DIR__ . '/tmp/';

    $frontend->path_data = __DIR__ . '/data/';

    // setup all Twig view dirs
    $frontend->addViewDir(__DIR__ . '/view/')
             ->addViewDir(__DIR__ . '/vendor/flood/canal-view/src')
             ->addViewDir(__DIR__ . '/data/content/_block', 'block');

    //
    // Add Features

    // storage
    $frontend->feature->add(require __DIR__ . '/vendor/flood/canal/feature/Storage/_feature.php');

    // route
    $route_config = [
        'debug'    => false,
        'api_base' => '/api',
    ];
    $route_feature = require __DIR__ . '/vendor/flood/canal/feature/feature-route.php';
    $frontend->feature->add($route_feature($route_config));

    // canal user
    $frontend->feature->add(require __DIR__ . '/vendor/flood/canal/feature/User/_feature.php');
    Token::$max_age = 30000;
    Token::$jwt_secret = 'vlQmc!:KcN]QpbxhJ1+RKzY,v%,$<cSA';

    // canal cms
    $frontend->feature->add(require __DIR__ . '/vendor/flood/canal/feature/Content/_feature.php');

    // twig
    $twig_config = [
        // If twig should check if need to re-cache, recommended to turn off in production
        'twig_auto_reload' => true,
    ];
    $twig_feature = require __DIR__ . '/vendor/flood/canal/feature/feature-twig.php';
    $frontend->feature->add($twig_feature($twig_config));

    // file manager (user upload files)
    $frontend->feature->add(require __DIR__ . '/vendor/flood/canal/feature/FileManager/_feature.php');

    // api
    $api_config = [
        'origin_allowed' => [
            'http://localhost:3000',
            'http://localhost:25021',
            'https://admin.demo.canal.bemit.codes',
            'https://nightly.admin.demo.canal.bemit.codes',
        ],
    ];
    $api_feature = require __DIR__ . '/vendor/flood/canal/feature/Api/_feature.php';
    $frontend->feature->add($api_feature($api_config));
};