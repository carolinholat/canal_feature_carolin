/**
 *
 */
export default class Menu {

    constructor(config) {
        this.config = $.extend({
            // defaults.
            hamburger: $('#nav-header--switch-trigger'),
            menu_mobile: $('.nav-header ul')
        }, config);
    }

    bind() {
        this.bindHandler();
    };

    bindHandler() {
        let $this = this;
        this.config.hamburger.on('click', function (event) {
            //event.preventDefault();
            $this.hamburger().toggle();
        });
    };

    /**
     *
     * @returns {Hamburger}
     */
    hamburger() {
        let $this = this;

        function Hamburger() {

        }

        Hamburger.prototype.toggle = function () {
            if ($this.config.hamburger.hasClass('open')) {
                $this.config.hamburger.addClass('close');
                $this.config.menu_mobile.addClass('close');
                $this.config.hamburger.removeClass('open');
                $this.config.menu_mobile.removeClass('open');
                setTimeout(function () {
                    $this.config.menu_mobile.css({
                        display: "none"
                    });
                }, 800);
            } else {
                $this.config.hamburger.addClass('open');
                $this.config.menu_mobile.addClass('open');
                $this.config.hamburger.removeClass('close');
                $this.config.menu_mobile.removeClass('close');
                $this.config.menu_mobile.css({
                    display: "block"
                });
            }
        };

        return new Hamburger();
    }
}