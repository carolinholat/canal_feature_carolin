#!/bin/bash

php ../vendor/phpmetrics/phpmetrics/bin/phpmetrics ../ --config="../_dev/phpmetrics.json" --report-html="../_dev/phpmetrics-result"

php ../vendor/phpmetrics/phpmetrics/bin/phpmetrics ../admin/ --config="../_dev/phpmetrics.json" --report-html="../_dev/phpmetrics-result--admin"
#php ./vendor/phpmetrics/phpmetrics/bin/phpmetrics ./ --extensions=php --report-html="./_dev/phpmetrics-result" --level=0 --excludedDirs="tmp" old v1 syntax